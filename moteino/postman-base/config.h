#define NODEID        1    // unique for each node on same network
#define NETWORKID     100  // the same on all nodes that talk to each other
#define FREQUENCY     RF69_433MHZ
#define ENCRYPTKEY    "uniqueEncryptKey" // 16 characters
#define IS_RFM69HW
#define ACK_TIME      50   // max # of ms to wait for an ack
#define LEDPIN        9    // Moteinos have LEDs on D9
#define SERIAL_BAUD   115200
