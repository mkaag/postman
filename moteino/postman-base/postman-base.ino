/*
  ** Postman Base **
  This sketch is based on Mailbox Notifier (http://lowpowerlab.com). 
  The Moteino is listening for incoming notification from Mailwatch Node
  and trigger notification to the Raspberry Pi.
 
  Creation date:  December 28, 2013 
  Author:         Maurice Kaag (https://github.com/mkaag/)
  Language:       Arduino
  License:        GPL v3 or later
 
  Circuit:
  * Moteino R4 with RFM69HW-433MHz transceiver and 4Mbit flash chip (http://lowpowerlab.com/moteino/)
  * Raspberry Pi model B (http://www.raspberrypi.org)
  
  Dependencies:
  * https://github.com/LowPowerLab/RFM69
  * https://github.com/LowPowerLab/SPIFlash
  * https://github.com/rocketscream/Low-Power

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <RFM69.h>
#include <SPI.h>
#include "config.h"

char temp[20];
RFM69 radio;

typedef struct 
{		
  unsigned long lastOpen;
  unsigned long lastClosed;
  unsigned short battery;
} Payload;
Payload theData;

byte battChar[8] = {0b00000, 0b01110, 0b11111, 0b11111, 0b11111, 0b11111, 0b11111, 0};

void setup() 
{
  Serial.begin(SERIAL_BAUD);
  delay(10);
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
#ifdef IS_RFM69HW
  radio.setHighPower();
#endif
  radio.encrypt(ENCRYPTKEY);

  char buff[50];
  sprintf(buff, "\nListening at %d Mhz...", FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
  Serial.println(buff);
}

byte recvCount = 0;
int milliCount=0;
void loop() 
{
  if (radio.receiveDone())
  {
    digitalWrite(LEDPIN, HIGH);
    
    Serial.print('[');Serial.print(radio.SENDERID, DEC);Serial.print("] ");
    Serial.print("to [");Serial.print(radio.TARGETID, DEC);Serial.print("] ");

    if (radio.DATALEN != sizeof(Payload))
      for (byte i = 0; i < radio.DATALEN; i++)
        Serial.print((char)radio.DATA[i]);
    else
    {
      theData = *(Payload*)radio.DATA; //assume radio.DATA actually contains our struct and not something else
      char periodO='X', periodC='X';
      long lastOpened = theData.lastOpen;
      long lastClosed = theData.lastClosed;
      long LO = lastOpened;
      long LC = lastClosed;
      char* MLOstr="LO:99d23h59m";
      char* MLCstr="LC:99d23h59m";
      char* BATstr="BAT:1024";
      
      if (lastOpened <= 59) periodO = 's'; //1-59 seconds
      else if (lastOpened <= 3599) { periodO = 'm'; lastOpened/=60; } //1-59 minutes
      else if (lastOpened <= 259199) { periodO = 'h'; lastOpened/=3600; } // 1-71 hours
      else if (lastOpened >= 259200) { periodO = 'd'; lastOpened/=86400; } // >=3 days
  
      if (lastClosed <= 59) periodC = 's';
      else if (lastClosed <= 3599) { periodC = 'm'; lastClosed/=60; }
      else if (lastClosed <= 259199) { periodC = 'h'; lastClosed/=3600; }
      else if (lastClosed >= 259200) { periodC = 'd'; lastClosed/=86400; }
      
      if (periodO == 'd')
        sprintf(MLOstr, "LO:%ldd%ldh", lastOpened, (LO%86400)/3600);
      else if (periodO == 'h')
        sprintf(MLOstr, "LO:%ldh%ldm", lastOpened, (LO%3600)/60);
      else sprintf(MLOstr, "LO:%ld%c", lastOpened, periodO);
  
      if (periodC == 'd')
        sprintf(MLCstr, "LC:%ldd%ldh", lastClosed, (LC%86400)/3600);
      else if (periodC == 'h')
        sprintf(MLCstr, "LC:%ldh%ldm", lastClosed, (LC%3600)/60);
      else sprintf(MLCstr, "LC:%ld%c", lastClosed, periodC);
      
      sprintf(BATstr, "BAT:%i", theData.battery);

      sprintf(temp, "MLO:%ld%c MLC:%ld%c", lastOpened, periodO, lastClosed, periodC);
      byte len = strlen(temp);
      Serial.print(temp); Serial.print(" ("); Serial.print(len); Serial.print(")"); 

      float battV = ((float)theData.battery * 3.3 * 9)/(1023*2.8776);
      dtostrf(battV, 3, 2, BATstr);

      Serial.print(BATstr);Serial.print("v");
      sprintf(temp, "%sv", BATstr);
    }
      
    Serial.print("   [RX_RSSI:");Serial.print(radio.RSSI);Serial.print("]");
    
    if (radio.ACK_REQUESTED)
    {
      byte theNodeID = radio.SENDERID;
      radio.sendACK();
      Serial.print(" - ACK sent.");
    }
    
    delay(5);
    digitalWrite(LEDPIN, LOW);
    Serial.println();
  }
}

