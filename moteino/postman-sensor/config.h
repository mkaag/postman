#define NODEID        2    // unique for each node on same network
#define NETWORKID     100  // the same on all nodes that talk to each other
#define GATEWAYID     1
#define FREQUENCY     RF69_433MHZ
#define ENCRYPTKEY    "uniqueEncryptKey" // 16 characters -same as base
#define IS_RFM69HW
#define ACK_TIME      50    // # of ms to wait for an ack
#define SENSORREADPERIOD  SLEEP_250MS  //this will cause transmissions every 2s
#define SENDINTERVAL   5000 // interval for sending readings without ACK
#define HALLSENSOR     A0
#define HALLSENSOR_EN  A1
#define BATTERYSENSE   A2
#define LEDPIN         9    // Moteinos have LEDs on D9
#define SERIAL_EN           // uncomment this line to enable serial IO debug messages
#define SERIAL_BAUD    115200
//#define BLINK_EN     //uncomment this to blink onboard LED every on every sensor reading
                       // WARNING: even though onboard LED is only 2ma, blinking will cause a magnitude higher current consumption         
