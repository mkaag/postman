/*
  ** Postman Sensor **
  This sketch is based on Mailbox Notifier (http://lowpowerlab.com) ; the hall sensor 
  will trigger notification to the Mailwatch Base.
 
  Creation date:  December 28, 2013 
  Author:         Maurice Kaag (https://github.com/mkaag/)
  Language:       Arduino
  License:        GPL v3 or later
 
  Circuit:
  * Moteino R4 with RFM69HW-433MHz transceiver and 4Mbit flash chip (http://lowpowerlab.com/moteino/)

  Dependencies:
  * https://github.com/LowPowerLab/RFM69
  * https://github.com/LowPowerLab/SPIFlash
  * https://github.com/rocketscream/Low-Power

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <RFM69.h>
#include <avr\sleep.h>
#include <avr\delay.h>
#include <LowPower.h>
#include <SPI.h>
#include "config.h"

#ifdef SERIAL_EN
  #define DEBUG(input)   {Serial.print(input); delay(1);}
  #define DEBUGln(input) {Serial.println(input); delay(1);}
#else
  #define DEBUG(input);
  #define DEBUGln(input);
#endif

typedef struct 
{                
  unsigned long lastOpen;
  unsigned long lastClosed;
  unsigned short battery;
} Payload;
Payload theData;

RFM69 radio;
char sendBuf[20];
byte sendLen;
byte temperatureCounter = 0;
float temperature = 0;
long doorPulseCount = 0;
unsigned long MLO=0, MLC=0; //MailLastOpen, MailLastClosed (ms)
unsigned long now = 0, lastSend = 0, temp = 0;

void setup() 
{
  Serial.begin(SERIAL_BAUD);
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
#ifdef IS_RFM69HW
  radio.setHighPower();
#endif
  radio.encrypt(ENCRYPTKEY);
  
  #ifdef SERIAL_EN
    Serial.begin(SERIAL_BAUD);
    char buff[50];
    sprintf(buff, "\nTransmitting at %d Mhz...", FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
    DEBUGln(buff);
  #endif
  
  pinMode(HALLSENSOR, INPUT);
  pinMode(HALLSENSOR_EN, OUTPUT);
  Blink(LEDPIN, 10);
  delay(100);
  Blink(LEDPIN, 10);
}

void loop() 
{
  byte reading = hallSensorReading();
  int batteryReading = analogRead(BATTERYSENSE);
  temp = millis();

  if (reading == 1)
  {
    if (++doorPulseCount == 1)
    {
      MLO = now; //save timestamp of event
      DEBUG("MAIL:O");
      sendLen = 6;
      
      //retry send up to 3 times when door event detected
      for(byte i=1; i<=3; i++)
      {
        //radio.Wakeup();  
        radio.send(GATEWAYID, "MAIL:O", sendLen, 1);
        DEBUG(" - waiting for ACK...");
        if (waitForAck())
        {
          DEBUG("ok!");
          break;
        }
        else DEBUG("nothing...");

        radio.sleep();
        LowPower.powerDown(SENSORREADPERIOD, ADC_OFF, BOD_ON); //retry every 2s until ACK received
      }
      radio.sleep();
      DEBUGln();
    }
  }
  else if (doorPulseCount >=1)
  {
    MLC = now; //save timestamp of event
    DEBUG("MAIL:C");
    sendLen = 6;

    //retry send up to 3 times when door event detected
    for(byte i=1; i<=3; i++)
    {
      //radio.Wakeup();
      radio.send(GATEWAYID, "MAIL:C", sendLen, 1);
      DEBUG(" - waiting for ACK...");
      if (waitForAck())
      {
        DEBUG("ok!");
        break;
      }
      else DEBUG("nothing...");

      radio.sleep();
      LowPower.powerDown(SENSORREADPERIOD, ADC_OFF, BOD_ON); //retry every 2s until ACK received
    }
    radio.sleep();
    doorPulseCount = 0; //reset counter
    DEBUGln();
  }
  
  //send readings every SENDINTERVAL
  if (now - lastSend > SENDINTERVAL)
  {
    char periodO='X', periodC='X';
    long lastOpened = (now - MLO) / 1000; //get seconds
    long lastClosed = (now - MLC) / 1000; //get seconds
    long LO = lastOpened;
    long LC = lastClosed;
    char* MLOstr="MLO:99d23h59m";
    char* MLCstr="MLC:99d23h59m";
    char* BATstr="BAT:1024";
    
    if (lastOpened <= 59) periodO = 's'; //1-59 seconds
    else if (lastOpened <= 3599) { periodO = 'm'; lastOpened/=60; } //1-59 minutes
    else if (lastOpened <= 259199) { periodO = 'h'; lastOpened/=3600; } // 1-71 hours
    else if (lastOpened >= 259200) { periodO = 'd'; lastOpened/=86400; } // >=3 days

    if (lastClosed <= 59) periodC = 's';
    else if (lastClosed <= 3599) { periodC = 'm'; lastClosed/=60; }
    else if (lastClosed <= 259199) { periodC = 'h'; lastClosed/=3600; }
    else if (lastClosed >= 259200) { periodC = 'd'; lastClosed/=86400; }
    
    if (periodO == 'd')
      sprintf(MLOstr, "MLO:%ldd%ldh", lastOpened, (LO%86400)/3600);
    else if (periodO == 'h')
      sprintf(MLOstr, "MLO:%ldh%ldm", lastOpened, (LO%3600)/60);
    else sprintf(MLOstr, "MLO:%ld%c", lastOpened, periodO);

    if (periodC == 'd')
      sprintf(MLCstr, "MLC:%ldd%ldh", lastClosed, (LC%86400)/3600);
    else if (periodC == 'h')
      sprintf(MLCstr, "MLC:%ldh%ldm", lastClosed, (LC%3600)/60);
    else sprintf(MLCstr, "MLC:%ld%c", lastClosed, periodC);
    
    sprintf(BATstr, "BAT:%i", batteryReading);
    sprintf(sendBuf, "%s %s %s", MLOstr, MLCstr, BATstr); //sprintf(sendBuf, "MLO:%ld%c MLC:%ld%c", lastOpened, periodO, lastClosed, periodC);
    sendLen = strlen(sendBuf);
    DEBUG(sendBuf); DEBUG(" ("); DEBUG(sendLen); DEBUGln(")");
    
    theData.lastOpen = LO;
    theData.lastClosed = LC;
    theData.battery = batteryReading;
    
    radio.send(GATEWAYID, (const void*)(&theData), sizeof(theData), 1); //0=noACK
    radio.sleep();
    lastSend = now;
  }
  
  #ifdef BLINK_EN
    Blink(LEDPIN, 5);
  #endif
      
  now = now + 250 + 22 + (millis()-temp); //correct millis(). Add 22ms to compensate time lost in other peripheral code, may need to be tweaked to be accurate
  LowPower.powerDown(SENSORREADPERIOD, ADC_OFF, BOD_ON);
}

// wait up to ACK_TIME for proper ACK, return true if received
static bool waitForAck() 
{
  long now = millis();
  while (millis() - now <= ACK_TIME)
    if (radio.ACKReceived(GATEWAYID))
      return true;
  return false;
}

byte hallSensorReading()
{
  digitalWrite(HALLSENSOR_EN, 1); //turn sensor ON
  delay(1); //wait a little
  byte reading = digitalRead(HALLSENSOR);
  digitalWrite(HALLSENSOR_EN, 0); //turn sensor OFF
  return reading;
}

void Blink(byte PIN, byte DELAY_MS)
{
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN, HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN, LOW);
}
