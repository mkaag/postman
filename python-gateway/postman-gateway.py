#!/usr/bin/env python
# -*- coding: latin-1 -*-

# Inspired from LowPowerLab and using the python-pushover module.
# https://github.com/Thibauth/python-pushover/
# https://github.com/LowPowerLab/MailboxNotifier/

import logging
import pushover
import serial
import string
import os, sys
import urllib, urllib2, urlparse
import time
from time import mktime
from datetime import datetime as dt, timedelta

# define the default com/serial and the baud rate used by the Moteino
SERIALPORT = '/dev/ttyAMA0'
BAUDRATE = 115200

# pushover.net configuration
PO_API = 'https://api.pushover.net/1/'
PO_TOKEN = ''
PO_MSG = 'Postman was there!'
PO_TITLE = 'Mailbox'
PO_USER = ''
PO_DEVICE = ''
PO_SOUND = 'bugle'
ENABLE_PO = True

# define last pushover
LAST_RUN = '2012-08-14 17:45:30'
RUN_INTERVAL = 5

# logging feature
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
handler = logging.FileHandler('/home/pi/postman-gateway/postman.log')
formatter = logging.Formatter('%(asctime)s - [%(levelname)s] %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

def pushOver():
        if ENABLE_PO:
		pushover.init(PO_TOKEN)
        	client = pushover.Client(PO_USER)
        	client.send_message(PO_MSG, title=PO_TITLE, priority=0, device=PO_DEVICE, sound=PO_SOUND)
	global LAST_RUN
	LAST_RUN = dt.now()

def checkLastRun():
	now = dt.now()
	if type(LAST_RUN) is not dt:
		last_run = now.strptime(LAST_RUN,'%Y-%m-%d %H:%M:%S')
	else:
		last_run = LAST_RUN
	elapsedTime = now - last_run
	elapsedMinutes = elapsedTime.total_seconds()/60

	logger.info('Trigger interval is set to %dmin', RUN_INTERVAL)
	logger.info('Last trigger happened at %s', LAST_RUN)
	if elapsedMinutes >= RUN_INTERVAL:
		return True
	else:
		return False

if __name__ == "__main__":
	logger.info('Start listening on %s at %d', SERIALPORT, BAUDRATE)

	try:
		ser = serial.Serial(SERIALPORT, BAUDRATE, timeout=10)
		while True:
        		line = ser.readline()
        		data = line.rstrip().split()  #no argument = split by whitespace
        		if len(data)>=2:
				logger.info(line)
            			for i in range(1, len(data)):
                			if data[i]=='MAIL:O':
						logger.info('Checking time of last run')
						if checkLastRun():
                					logger.info('Sending pushover notification')
							pushOver()
	except (SystemExit, KeyboardInterrupt):
		ser.close()
		print ''
    		logger.info('User interrupt, shutting down...')
	except Exception, e:
    		logger.error('Something weird happened, check your config...', exc_info=True)
